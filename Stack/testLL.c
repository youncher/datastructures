#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main() {

    Stack * myStack = malloc(sizeof(Stack));
    s_push(myStack,'a');
    s_push(myStack,'b');
    s_push(myStack,'c');
    s_push(myStack,'d');
    s_push(myStack,'e');
    s_push(myStack,'f');
    s_push(myStack,'g');

    s_print(myStack);
    printf("\n");

    s_pop(myStack);
    s_pop(myStack);
    s_pop(myStack);
    s_pop(myStack);


    s_print(myStack);
    printf("\n");    

    
}



