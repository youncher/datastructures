# Data Structure in C Language

This repo is a C implementation of data structures, containing our custom libraries. We aim to support and ease the usage and understanding of data structure implementation for beginners like us :)

## Installation

No installation required. Just include the header files you need in the main code.


## Usage Example

```
#include "stack.h"

int main() {
...

//Creating a new stack
Stack * myStack = malloc(sizeof(Stack));

//Push
s_push(myStack,'b');

//Pop
s_pop(myStack);

//Handle the value of pop
char output = s_pop(myStack);

//Printing
s_print(myStack);

...
}

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)