#ifndef STACK
#define STACK

typedef struct nodeS{
    char value;
    struct nodeS *next;
} NodeStack;

typedef struct stack{
    NodeStack * top;
} Stack;

void s_push(Stack *stack, char data);

char s_pop(Stack *stack);

void s_print(Stack *stack);

#endif