#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

void s_push(Stack *stack, char data){
    if(stack==NULL){
        (stack->top)->value=data;
        (stack->top)->next=NULL;
        return;
    }

    NodeStack *newNode= malloc(sizeof(NodeStack));
    newNode->value=data;
    newNode->next=stack->top;
    stack->top=newNode;
    
}

char s_pop(Stack *stack){
    if(stack->top==NULL)
        return NULL;
    
    NodeStack *currentNode = stack->top;
    NodeStack *nextNode = currentNode->next;

    char tmp = currentNode->value;
    if(nextNode==NULL)
        stack->top=NULL;
    else
        stack->top=nextNode;

    free(currentNode);
    return tmp;
}

void s_print(Stack *stack){
    NodeStack * top=stack->top;
    if(top==NULL){
        printf("Empty\n");
        return;
    }
    do{
        printf("%c\t",(top)->value);
        (top)=(top)->next;
    }while(top!=NULL);
   
    printf("\n");
}


