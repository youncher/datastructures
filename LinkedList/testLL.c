#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"

int main() {

    NodeLL *linkedListPtr=NULL;
    
    linkedListPtr = ll_add(linkedListPtr,'a');
    linkedListPtr = ll_add(linkedListPtr,'b');
    linkedListPtr = ll_add(linkedListPtr,'c');
    linkedListPtr = ll_add(linkedListPtr,'d');
    linkedListPtr = ll_add(linkedListPtr,'e');
    linkedListPtr = ll_add(linkedListPtr,'f');
    linkedListPtr = ll_add(linkedListPtr,'g');
    ll_printList(linkedListPtr);

    linkedListPtr=ll_delete(linkedListPtr,'g');
    ll_printList(linkedListPtr);
    linkedListPtr=ll_delete(linkedListPtr,'b');
    ll_printList(linkedListPtr);
    linkedListPtr=ll_delete(linkedListPtr,'a');
    ll_printList(linkedListPtr);
    linkedListPtr=ll_delete(linkedListPtr,'f');
    ll_printList(linkedListPtr);
    linkedListPtr=ll_delete(linkedListPtr,'e');
    ll_printList(linkedListPtr);
    linkedListPtr=ll_delete(linkedListPtr,'d');
    ll_printList(linkedListPtr);
    linkedListPtr=ll_delete(linkedListPtr,'c');
    ll_printList(linkedListPtr);

    
}



