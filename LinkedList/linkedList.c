#include "linkedList.h"
#include <stdio.h>
#include <stdlib.h>

/*NodeLL* ll_add(NodeLL *head, char data){
    NodeLL *newNode=NULL;
    newNode = malloc(sizeof(NodeLL));
    newNode->value=data;
    newNode->next=head;
    return newNode;
}*/

NodeLL* ll_add(NodeLL *head, char data){
    NodeLL *newNode=NULL;
    newNode = malloc(sizeof(NodeLL));
    newNode->value=data;
    newNode->next=head;
    return newNode;
}

NodeLL* ll_delete(NodeLL *head, char charToDelete){
    NodeLL *previousNode = NULL;
    NodeLL *currentNode = head;
    while(head != NULL){
        if(currentNode->value==charToDelete){
            if(previousNode==NULL){
                head=currentNode->next;
                free(currentNode);
                return head;
            }
            if(currentNode->next==NULL){
                previousNode->next=NULL;
                free(currentNode);
                return head;
            }
            previousNode->next = currentNode->next;                
            free(currentNode);
            return head;
        }
        previousNode = currentNode;
        currentNode = currentNode ->next;
    }
    return head;
}

void ll_printList(NodeLL *head){
    while(head!=NULL){
        printf("%c\t",head->value);
        head=head->next;
    }
    printf("\n");
}


