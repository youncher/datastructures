#ifndef LINKEDLIST
#define LINKEDLIST

typedef struct nodell{
    char value;
    struct nodell *next;
} NodeLL;

NodeLL* ll_add(NodeLL *head, char data);

NodeLL* ll_delete(NodeLL *head, char charToDelete);

void ll_printList(NodeLL *head);

#endif