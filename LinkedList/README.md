# Data Structure in C Language

This repo is a C implementation of data structures, containing our custom libraries. We aim to support and ease the usage and understanding of data structure implementation for beginners like us :)

## Installation

No installation required. Just include the header files you need in the main code.


## Usage Example

```
#include "linkedList.h"

int main() {
...

//Creating a new list
NodeLL *linkedListPtr=NULL;

//Adding
linkedListPtr = ll_add(linkedListPtr,'a');

//Deleting
linkedListPtr=ll_delete(linkedListPtr,'g');

//Printing
ll_printList(linkedListPtr);

...
}

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)