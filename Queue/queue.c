#include "queue.h"
#include <stdio.h>
#include <stdlib.h>

Queue * q_enqueue(Queue *queue, char data){
    NodeQ *newNode=NULL;
    newNode = malloc(sizeof(NodeQ));
    newNode->value=data;
    newNode->next=NULL;

    if(queue->head==NULL){
        queue->tail=newNode;
        queue->head=queue->tail;
        return queue;
    }

    (queue->tail)->next=newNode;
    queue->tail=newNode;
    return queue;
}

char q_dequeue(Queue *queue){    
    
    if(queue->head==NULL)
        return NULL;

    NodeQ * nodeToPop = queue->head;

    if(nodeToPop->next!=NULL)
        queue->head = nodeToPop->next;
    
    char tmpValue = nodeToPop->value;
    free(nodeToPop);
    return tmpValue;

}

void q_printQueue(Queue *queue){
    NodeQ *tmp=queue->head;
    if(tmp==NULL){
        printf("Empty\n");
        return;
    }
        
    do{
        printf("%c\t",(tmp)->value);
        (tmp)=(tmp)->next;
    }while(tmp!=NULL);
    
    printf("\n");
}