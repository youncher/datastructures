#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

int main() {
    Queue * myQueue = malloc(sizeof(Queue));
    myQueue = q_enqueue(myQueue,'a');
    myQueue = q_enqueue(myQueue,'b');
    myQueue = q_enqueue(myQueue,'c');
    myQueue = q_enqueue(myQueue,'d');
    myQueue = q_enqueue(myQueue,'e');
    myQueue = q_enqueue(myQueue,'f');
    myQueue = q_enqueue(myQueue,'g');

    q_printQueue(myQueue);
    printf("\n");

    q_dequeue(myQueue);
    q_dequeue(myQueue);
    q_dequeue(myQueue);

    q_printQueue(myQueue);
    printf("\n");    

}



