# Data Structure in C Language

This repo is a C implementation of data structures, containing our custom libraries. We aim to support and ease the usage and understanding of data structure implementation for beginners like us :)

## Installation

No installation required. Just include the header files you need in the main code.


## Usage Example

```
#include "queue.h"

int main() {
...

//Creating a new queue
Queue * myQueue = malloc(sizeof(Queue));

//Enqueue
myQueue = q_enqueue(myQueue,'a');

//Dequeue
q_dequeue(myQueue);

//Handle the value of dequeue
char output = q_dequeue(myQueue);

//Printing
q_printQueue(myQueue);

...
}

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)