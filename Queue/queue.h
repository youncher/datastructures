#ifndef QUEUE
#define QUEUE

typedef struct nodeq{
    char value;
    struct nodeq *next;
} NodeQ;

typedef struct queue{
    NodeQ * head;
    NodeQ * tail;
} Queue;

Queue * q_enqueue(Queue *queue, char data);

char q_dequeue(Queue *queue);

void q_printQueue(Queue *queue);
//void cdll_reversePrintList(NodeQ *head);

#endif