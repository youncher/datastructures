#include "doubleLinkedList.h"
#include <stdio.h>
#include <stdlib.h>

NodeDLL* dll_add(NodeDLL *head, char data){
    NodeDLL *newNode=NULL;
    newNode = malloc(sizeof(NodeDLL));
    newNode->value=data;
    newNode->next=head;
    newNode->prev=NULL;
    if(head==NULL)
        return newNode;
    head->prev=newNode;    
    return newNode;
}

NodeDLL* dll_delete(NodeDLL *head, char charToDelete){
    NodeDLL *previousNode = NULL;
    NodeDLL *currentNode = head;

    while(head!=NULL){
        NodeDLL *nextNode=NULL;
        if(currentNode->next!=NULL)
            nextNode=currentNode->next;

        if(currentNode->value==charToDelete){
            if(currentNode==head && currentNode->next==NULL){
                head=NULL;
                free(currentNode);
                return head;
            }                

            if(previousNode==NULL){
                nextNode->prev=NULL;
                head=nextNode;
                free(currentNode);
                return head;
            }

            if(currentNode->next==NULL){
                previousNode->next=NULL;
                free(currentNode);
                return head;
            }

            previousNode->next = currentNode->next;   
            nextNode->prev = currentNode->prev;
            free(currentNode);
            return head;
        }

        previousNode = currentNode;
        if(currentNode->next!=NULL)
            currentNode = currentNode ->next;
    };
    return head;
}

void dll_printList(NodeDLL *head){
    while(head!=NULL){
        printf("%c\t",head->value);
        head=head->next;
    }
    printf("\n");
}

void dll_printListR(NodeDLL *head){
    while(head!=NULL && head->next!=NULL){
        printf("%c\t",head->value);

        head=head->next;
    }
    //head=head->prev;
    while(head!=NULL){
        printf("%c\t",head->value);
        head=head->prev;
    }
    printf("\n");
}


