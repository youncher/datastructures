#include <stdio.h>
#include <stdlib.h>
#include "doubleLinkedList.h"

int main() {

    NodeDLL *doubleLinkedListPtr=NULL;
    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'g');
    
    doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'a');
    doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'b');
    doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'c');
    doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'d');
    doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'e');
    doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'f');
    doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'g');
    dll_printList(doubleLinkedListPtr);
    dll_printListR(doubleLinkedListPtr);

    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'g');
    dll_printList(doubleLinkedListPtr);
    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'b');
    dll_printList(doubleLinkedListPtr);
    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'a');
    dll_printList(doubleLinkedListPtr);
    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'d');
    dll_printList(doubleLinkedListPtr);
    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'c');
    dll_printList(doubleLinkedListPtr);
    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'f');
    dll_printList(doubleLinkedListPtr);
    doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'e');
    dll_printList(doubleLinkedListPtr);

}



