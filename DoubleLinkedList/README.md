# Data Structure in C Language

This repo is a C implementation of data structures, containing our custom libraries. We aim to support and ease the usage and understanding of data structure implementation for beginners like us :)

## Installation

No installation required. Just include the header files you need in the main code.


## Usage Example

```
#include "doubleLinkedList.h"

int main() {
...

//Creating a new list
NodeDLL *doubleLinkedListPtr=NULL;

//Adding
doubleLinkedListPtr = dll_add(doubleLinkedListPtr,'a');

//Deleting
doubleLinkedListPtr=dll_delete(doubleLinkedListPtr,'g');

//Printing
dll_printList(doubleLinkedListPtr);

//Reverse printing
dll_printListR(doubleLinkedListPtr);

...
}

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)