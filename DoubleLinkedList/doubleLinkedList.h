#ifndef DOUBLELINKEDLIST
#define DOUBLELINKEDLIST

typedef struct nodedll{
    char value;
    struct nodedll *next;
    struct nodedll *prev;
} NodeDLL;

NodeDLL* dll_add(NodeDLL *head, char data);

NodeDLL* dll_delete(NodeDLL *head, char charToDelete);

void dll_printList(NodeDLL *head);
void dll_printListR(NodeDLL *head);

#endif