#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

int main() {

    BSTree * myTree = malloc(sizeof(BSTree));
    insert(myTree,5);
    insert(myTree,4);
    insert(myTree,3);
    insert(myTree,2);
    insert(myTree,8);
    insert(myTree,9);
    insert(myTree,1);
    insert(myTree,12);
    insert(myTree,17);
    insert(myTree,21);
    insert(myTree,26);



    printf("%d\n",myTree->root->value);
    printf("%d\n",((myTree->root)->left)->value);
    printf("%d\n",((myTree->root)->right)->value);
    printf("%d\n",((myTree->root)->right->right)->value);
    printf("\n");

    delete(myTree,4);

    printf("%d\n",myTree->root->value);
    printf("%d\n",((myTree->root)->left)->value);
    printf("%d\n",((myTree->root)->right)->value);
    printf("%d\n",((myTree->root)->right->right)->value);
    printf("\n");

    delete(myTree,3);

    printf("%d\n",myTree->root->value);
    printf("%d\n",((myTree->root)->left)->value);
    printf("%d\n",((myTree->root)->right)->value);
    printf("%d\n",((myTree->root)->right->right)->value);

    printNode(myTree->root);

    
}



