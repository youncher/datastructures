#ifndef BSTREE
#define BSTREE

typedef struct nodeT{
    int value;
    struct nodeT *left;
    struct nodeT *right;
} NodeTree;

typedef struct tree{
    NodeTree * root;
} BSTree;;

void insert(BSTree *tree, int value);
void insertNode(NodeTree *subRoot, int value);

#endif