#include "tree.h"
#include <stdio.h>
#include <stdlib.h>

NodeTree * createNode(int value){
    NodeTree * newNode = malloc(sizeof(NodeTree));
    newNode->value=value;
    newNode->right=NULL;
    newNode->left=NULL;
    return newNode;
}

void insert(BSTree *tree, int value){
    if(tree->root==NULL){
        tree->root=createNode(value);
        return;
    }

    insertNode(tree->root,value);
}

void insertNode(NodeTree *subRoot, int value){
    if(subRoot==NULL){ // root = NULL
        printf("It is null, the value %d will be added...\n",value);
        NodeTree * newNode = createNode(value);        
        subRoot=newNode;
    }

    if(value < (subRoot->value)){
        //printf("Entering the left of %d...", subRoot->value);
        if(subRoot->left==NULL){
            NodeTree * newNode = createNode(value);
            subRoot->left=newNode;       
        }else{
            insertNode(subRoot->left,value);   
        }
    }
    if(value > (subRoot->value)){
        //printf("Entering the right of %d...", subRoot->value);
        if(subRoot->right==NULL){
            NodeTree * newNode = createNode(value);
            subRoot->right=newNode;       
        }else{
            insertNode(subRoot->right,value);   
        }
    }

    if(subRoot->value==value){
        printf("This value already exists.\n");
        return;
    }
    
}


NodeTree * deleteNode(NodeTree *toRemove){
    NodeTree *toFree;
    if(toRemove->left==NULL){
        toFree=toRemove;
        toRemove= toRemove->right;
        
    }else if(toRemove->right==NULL){
        toFree=toRemove;
        toRemove=toRemove->left;
    }else{
        NodeTree * leftOf = toRemove->left;
        NodeTree * rightOf = toRemove->right;
        append(leftOf,rightOf);
        toFree=toRemove;
        toRemove=leftOf;
    }
    free(toFree);
    return toRemove;

}

void append(NodeTree * main, NodeTree *branch){
    //NodeTree *tmp=main;
    while(main->right!=NULL){
        main=main->right;
    }
    main->right=branch;
}

void delete(BSTree *tree,int intToRemove){
    if(intToRemove == tree->root->value && tree->root != NULL){
        tree->root=deleteNode(tree->root);
        return;
    }
    findAndDelete(tree->root,intToRemove);
}

void findAndDelete(NodeTree *subRoot,int intToRemove){
    if(intToRemove<subRoot->value){
        if(subRoot->left->value==intToRemove){
            subRoot->left=deleteNode(subRoot->left);
            return;
        }
        findAndDelete(subRoot->left,intToRemove);
        return;
    }

    if(intToRemove>subRoot->value){
        if(subRoot->right->value==intToRemove){
            subRoot->right=deleteNode(subRoot->right);
            return;
        }
        findAndDelete(subRoot->right,intToRemove);
        return;
    }
}

void printNode(NodeTree *subRoot){
    if(subRoot==NULL)
        return;

    if(subRoot->left!=NULL){
        printNode(subRoot->left);
    }
    printf("%d\t",subRoot->value);
    if(subRoot->right!=NULL){
        printNode(subRoot->right);
    }

}
