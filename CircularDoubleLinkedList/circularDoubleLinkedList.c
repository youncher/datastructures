#include "circularDoubleLinkedList.h"
#include <stdio.h>
#include <stdlib.h>

NodeCDLL* cdll_add(NodeCDLL *head, char data){
    NodeCDLL *newNode=NULL;
    newNode = malloc(sizeof(NodeCDLL));
    newNode->value=data;
    newNode->next=head;   

    if(head==NULL){
        newNode->prev=newNode;
        return newNode;
    }
    newNode->prev=head->prev;
    (newNode->prev)->next=newNode;
    head->prev=newNode;
    return newNode;
}

NodeCDLL* cdll_delete(NodeCDLL *head, char charToDelete){    
    NodeCDLL *currentNode = head;

    while(head!=NULL){
        NodeCDLL *previousNode = NULL;
        NodeCDLL *nextNode=NULL;
        
        nextNode=currentNode->next;
        previousNode=currentNode->prev;

        if(currentNode->value==charToDelete){
            if(currentNode==head && currentNode->next==NULL){
                head=NULL;
                free(currentNode);
                return head;
            }

            if(previousNode==NULL){
                nextNode->prev=NULL;
                head=nextNode;
                free(currentNode);               
                return head;
            }

            if(currentNode->next==NULL){
                previousNode->next=NULL;
                free(currentNode);
                return head;
            }

            previousNode->next = currentNode->next;   
            nextNode->prev = currentNode->prev;
            head=nextNode;
            free(currentNode);
            return head;
        }

        previousNode = currentNode;
        if(currentNode->next!=NULL)
            currentNode = currentNode ->next;
    };
    return head;
}

void cdll_printList(NodeCDLL *head){
    NodeCDLL *tmp=head;
    if(head==NULL){
        printf("Empty\n");
        return;
    }
        
    do{
        printf("%c\t",head->value);
        head=head->next;
    }while(head!=tmp);
    printf("\n");
}

void cdll_reversePrintList(NodeCDLL *head){
    NodeCDLL *tmp=head;
    if(head==NULL){
        printf("Empty\n");
        return;
    }

    do{
        printf("%c\t",head->value);
        head=head->prev;
    }while(head!=tmp);
    printf("\n");
}


