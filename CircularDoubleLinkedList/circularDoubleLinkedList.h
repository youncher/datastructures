#ifndef CIRCULARDOUBLELINKEDLIST
#define CIRCULARDOUBLELINKEDLIST

typedef struct nodecdll{
    char value;
    struct nodecdll *next;
    struct nodecdll *prev;
} NodeCDLL;

NodeCDLL* cdll_add(NodeCDLL *head, char data);

NodeCDLL* cdll_delete(NodeCDLL *head, char charToDelete);

void cdll_printList(NodeCDLL *head);
void cdll_reversePrintList(NodeCDLL *head);

#endif